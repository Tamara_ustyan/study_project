CREATE TABLE article
(
    id       BIGSERIAL PRIMARY KEY,
    name     TEXT NOT NULL,
    tags     TEXT[] NOT NULL,
    comments TEXT[] NOT NULL
);