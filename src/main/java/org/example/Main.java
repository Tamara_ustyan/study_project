package org.example;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import org.flywaydb.core.Flyway;

public class Main {
    public static void main(String[] args) {
        Config config = ConfigFactory.load();

        Flyway flyway =
            Flyway.configure()
                .outOfOrder(true)
                .locations("classpath:db.migrations")
                .dataSource(config.getString("app.database.url"), config.getString("app.database.user"),
                    config.getString("app.database.password"))
                .load();
        flyway.migrate();
    }
}