package service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import controller.dto.request.ArticleUpdateRequest;
import controller.dto.response.ArticleResponse;
import entity.Article;
import entity.Article.ArticleId;
import entity.Comment;
import entity.Comment.CommentId;
import repository.ArticleRepository;
import repository.CommentRepository;
import repository.exception.ArticleNotFoundException;
import service.exception.ArticleDeleteException;
import service.exception.ArticleUpdateException;

public class ArticleService {
    private final ArticleRepository articleRepository;
    private final CommentRepository commentRepository;
    private final CommentService commentService;
    private final TransactionManager transactionManager;

    public ArticleService(ArticleRepository articleRepository,
                          CommentRepository commentRepository,
                          CommentService commentService,
                          TransactionManager transactionManager) {
        this.articleRepository = articleRepository;
        this.commentRepository = commentRepository;
        this.commentService = commentService;
        this.transactionManager = transactionManager;
    }

    public List<ArticleResponse> findAll() {
        return articleRepository.findAll().stream().map(ArticleResponse::new).toList();
    }

    public ArticleResponse findById(ArticleId articleId) {
        return transactionManager.inTransaction(() -> new ArticleResponse(articleRepository.findById(articleId)));
    }

    public long create(String name) {
        return transactionManager.inTransaction(() -> {
            final var article = articleRepository.create(name);
            return article.getValue();
        });
    }

    public Set<ArticleId> createArticles(Set<String> names) {
        return transactionManager.inTransaction(() -> {
            Set<ArticleId> result = new HashSet<>();
            for (String name : names) {
                final var article = articleRepository.create(name);
                result.add(article);
            }
            return result;
        });
    }

    public void update(ArticleUpdateRequest articleUpdateRequest) {
        transactionManager.useTransaction(() -> {
            Article article;
            try {
                article = articleRepository.findById(articleUpdateRequest.articleId());
            } catch (ArticleNotFoundException e) {
                throw new ArticleUpdateException("Cannot find article with id=" + articleUpdateRequest.articleId(), e);
            }
            articleRepository.update(
                article.withName(articleUpdateRequest.name())
                    .withTags(articleUpdateRequest.tags())
                    .withComments(articleUpdateRequest.comments().stream().map(commentRepository::findById).toList())
            );
        });
    }

    public void addNewComment(ArticleId articleId, CommentId commentId) {
        transactionManager.useTransaction(() -> {
            Article article = articleRepository.findById(articleId);
            Comment comment = commentRepository.findById(commentId);
            final var newComments = article.addComment(comment);
            update(new ArticleUpdateRequest(articleId, article.getName(), article.getTags(), newComments));
        });
    }

    public void deleteComment(ArticleId articleId, CommentId commentId) {
        transactionManager.useTransaction(() -> {
            Article article = articleRepository.findById(articleId);
            final var newComments = article.removeComment(commentId);
            update(new ArticleUpdateRequest(articleId, article.getName(), article.getTags(), newComments));
            commentService.delete(commentId);
        });
    }

    public void delete(ArticleId articleId) {
        transactionManager.useTransaction(() -> {
            try {
                articleRepository.delete(articleId);
            } catch (ArticleNotFoundException e) {
                throw new ArticleDeleteException("Cannot delete article with id=" + articleId, e);
            }
        });
    }
}
