package service.exception;

import repository.exception.CommentIdDuplicatedException;

public class CommentCreateException extends RuntimeException {
    public CommentCreateException(String message, CommentIdDuplicatedException e) {
        super(message);
    }
}
