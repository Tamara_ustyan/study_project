package service.exception;

import repository.exception.ArticleNotFoundException;

public class ArticleUpdateException extends RuntimeException {
    public ArticleUpdateException(String message, ArticleNotFoundException e) {
        super(message);
    }
}