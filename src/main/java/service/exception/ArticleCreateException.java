package service.exception;

import repository.exception.ArticleIdDuplicatedException;

public class ArticleCreateException extends RuntimeException {
    public ArticleCreateException(String message, ArticleIdDuplicatedException e) {
        super(message);
    }
}
