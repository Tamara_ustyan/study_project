package service.exception;

import repository.exception.CommentNotFoundException;

public class CommentDeleteException extends RuntimeException {
    public CommentDeleteException(String message, CommentNotFoundException e) {
        super(message);
    }
}
