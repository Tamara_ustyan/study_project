package service.exception;

import repository.exception.ArticleNotFoundException;

public class ArticleDeleteException extends RuntimeException {
    public ArticleDeleteException(String message, ArticleNotFoundException e) {
        super(message);
    }
}
