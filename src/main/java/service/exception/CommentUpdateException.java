package service.exception;

import repository.exception.CommentNotFoundException;

public class CommentUpdateException extends RuntimeException {
    public CommentUpdateException(String message, CommentNotFoundException e) {
        super(message);
    }
}
