package service.exception;

import repository.exception.ArticleNotFoundException;

public class ArticleFindException extends RuntimeException {
    public ArticleFindException(String message, ArticleNotFoundException e) {
        super(message);
    }
}
