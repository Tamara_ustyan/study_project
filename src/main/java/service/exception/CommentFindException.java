package service.exception;

import repository.exception.CommentNotFoundException;

public class CommentFindException extends RuntimeException {
    public CommentFindException(String message, CommentNotFoundException e) {
        super(message);
    }
}
