package service;

import java.util.List;

import controller.dto.request.CommentUpdateRequest;
import controller.dto.response.CommentResponse;
import entity.Article.ArticleId;
import entity.Comment;
import entity.Comment.CommentId;
import repository.CommentRepository;
import repository.exception.CommentNotFoundException;
import service.exception.CommentDeleteException;
import service.exception.CommentFindException;
import service.exception.CommentUpdateException;

public class CommentService {
    private final CommentRepository commentRepository;
    private final TransactionManager transactionManager;

    public CommentService(CommentRepository commentRepository, TransactionManager transactionManager) {
        this.commentRepository = commentRepository;
        this.transactionManager = transactionManager;
    }

    public CommentId create(ArticleId articleId, String text) {
        return transactionManager.inTransaction(() -> commentRepository.create(text, articleId));
    }

    public CommentResponse findById(CommentId commentId) {
        try {
            return transactionManager.inTransaction(() -> new CommentResponse(commentRepository.findById(commentId)));
        } catch (CommentNotFoundException e) {
            throw new CommentFindException("Cannot find comment by id=" + commentId, e);
        }
    }

    public List<CommentResponse> findAll() {
        return commentRepository.findAll().stream().map(CommentResponse::new).toList();
    }

    public void delete(CommentId commentId) {
        try {
            commentRepository.delete(commentId);
        } catch (CommentNotFoundException e) {
            throw new CommentDeleteException("Cannot delete comment with id=" + commentId, e);
        }
    }

    public void update(CommentUpdateRequest commentUpdateRequest) {
        transactionManager.useTransaction(() -> {
            Comment comment;
            try {
                comment = commentRepository.findById(commentUpdateRequest.commentId());
            } catch (CommentNotFoundException e) {
                throw new CommentUpdateException("Cannot find comment with id=" + commentUpdateRequest.commentId(), e);
            }
            commentRepository.update(comment.withText(commentUpdateRequest.text()));
        });
    }
}
