package repository;

import java.util.List;

import entity.Article;
import entity.Article.ArticleId;

public interface ArticleRepository {
    List<Article> findAll();

    /**
     * @throws ArticleNotFoundException
     */
    Article findById(ArticleId articleId);

    /**
     * @throws ArticleIdDuplicatedException
     */
    ArticleId create(String name);

    /**
     * @throws ArticleNotFoundException
     */
    void update(Article article);

    /**
     * @throws ArticleNotFoundException
     */
    void delete(ArticleId articleId);
}
