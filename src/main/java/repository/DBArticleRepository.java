package repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.array.SqlArrayMapperFactory;
import org.jdbi.v3.core.result.ResultBearing;
import org.postgresql.util.PGobject;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import entity.Article;
import entity.Article.ArticleId;
import entity.Comment;
import repository.exception.ArticleCreateException;
import repository.exception.ArticleNotFoundException;
import repository.exception.ArticleUpdateException;

public class DBArticleRepository implements ArticleRepository {
    private final Jdbi jdbi;

    public DBArticleRepository(Jdbi jdbi) {
        this.jdbi = jdbi;
    }

    @Override
    public List<Article> findAll() {
        try {
            return jdbi.inTransaction((Handle handle) -> {
                List<Map<String, Object>> results =
                    handle.createQuery("SELECT id, name, tags, comments FROM article")
                        .registerColumnMapper(new SqlArrayMapperFactory())
                        .mapToMap()
                        .list();
                List<Article> articles = new ArrayList<>();

                for (Map<String, Object> result : results) {
                    Set<String> tags = new HashSet<>();
                    if (result.get("tags") != null) {
                        Array tagsArray = (Array) result.get("tags");
                        tags.addAll(Arrays.asList((String[]) tagsArray.getArray()));
                    }

                    List<Comment> comments = new ArrayList<>();
                    PGobject commentsPGObject = (PGobject) result.get("comments");
                    if (commentsPGObject != null) {
                        String commentsString = commentsPGObject.getValue();
                        if (commentsString != null && !commentsString.isEmpty()) {
                            ObjectMapper mapper = new ObjectMapper();
                            List<Comment> parsedComments = mapper.readValue(commentsString, new TypeReference<>() {});
                            comments.addAll(parsedComments);
                        }
                    }
                    articles.add(new Article(
                        new ArticleId((Long) result.get("id")),
                        ((String) result.get("name")),
                        tags,
                        comments
                    ));
                }
                return articles;
            });
        } catch (Exception e) {
            throw new ArticleNotFoundException(e.getMessage());
        }
    }

    @Override
    public Article findById(ArticleId articleId) {
        try {
            return jdbi.inTransaction((Handle handle) -> {
                Map<String, Object> result =
                    handle.createQuery("SELECT id, name, tags, comments FROM article WHERE id = :id")
                        .bind("id", articleId.getValue())
                        .registerColumnMapper(new SqlArrayMapperFactory())
                        .mapToMap()
                        .first();

                Set<String> tags = new HashSet<>();
                if (result.get("tags") != null) {
                    Array tagsArray = (Array) result.get("tags");
                    tags.addAll(Arrays.asList((String[]) tagsArray.getArray()));
                }

                List<Comment> comments = new ArrayList<>();
                PGobject commentsPGObject = (PGobject) result.get("comments");
                if (commentsPGObject != null) {
                    String commentsString = commentsPGObject.getValue();
                    if (commentsString != null && !commentsString.isEmpty()) {
                            ObjectMapper mapper = new ObjectMapper();
                            List<Comment> parsedComments = mapper.readValue(commentsString, new TypeReference<>() {});
                            comments.addAll(parsedComments);
                    }
                }

                return new Article(
                    new ArticleId((Long) result.get("id")),
                    ((String) result.get("name")),
                    tags,
                    comments
                );
            });
        } catch (Exception e) {
            throw new ArticleNotFoundException(e.getMessage());
        }
    }

    @Override
    public ArticleId create(String name) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return jdbi.inTransaction((Handle handle) -> {
                ResultBearing resultBearing = handle.createUpdate(
                        "INSERT INTO article (name, tags, comments) VALUES (:name, :tags, CAST(:comments AS JSONB))")
                                                  .bind("name", name)
                                                  .bind("tags", new String[0])
                                                  .bindByType("comments", objectMapper.writeValueAsString(new Comment[0]), String.class)
                                                  .executeAndReturnGeneratedKeys("id");
                Map<String, Object> mapResult = resultBearing.mapToMap().first();
                return new ArticleId((Long) mapResult.get("id"));
            });
        } catch (JsonProcessingException e) {
            throw new ArticleCreateException(e.getMessage());
        }
    }

    @Override
    public void update(Article article) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            jdbi.inTransaction((Handle handle) ->
                                   handle.createUpdate("UPDATE article SET name = :name, tags = :tags, comments = :comments::jsonb WHERE id = :id")
                                       .bind("id", article.getId().getValue())
                                       .bind("name", article.getName())
                                       .bind("tags", article.getTags().toArray(new String[0]))
                                       .bind("comments", objectMapper.writeValueAsString(article.getComments()))
                                       .execute()
            );
        } catch (JsonProcessingException e) {
            throw new ArticleUpdateException(e.getMessage());
        }
    }

    @Override
    public void delete(ArticleId articleId) {
        jdbi.inTransaction((Handle handle) ->
            handle.createUpdate("DELETE FROM article WHERE id = :id")
                .bind("id", articleId.getValue())
                .execute()
        );
    }
}
