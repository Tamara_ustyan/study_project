package repository;

import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.array.SqlArrayMapperFactory;
import org.jdbi.v3.core.result.ResultBearing;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import entity.Article.ArticleId;
import entity.Comment;
import entity.Comment.CommentId;
import repository.exception.ArticleNotFoundException;

public class DBCommentRepository implements CommentRepository {
    private final Jdbi jdbi;

    public DBCommentRepository(Jdbi jdbi) {
        this.jdbi = jdbi;
    }

    @Override
    public List<Comment> findAll() {
        try {
            return jdbi.inTransaction((Handle handle) -> {
                List<Map<String, Object>> results =
                    handle.createQuery("SELECT id, article_id, text FROM comment")
                        .registerColumnMapper(new SqlArrayMapperFactory())
                        .mapToMap()
                        .list();
                List<Comment> comments = new ArrayList<>();

                for (Map<String, Object> result : results) {
                    comments.add(new Comment(
                        new CommentId((Long) result.get("id")),
                        new ArticleId((Long) result.get("article_id")),
                        (String) result.get("text")
                    ));
                }
                return comments;
            });
        } catch (Exception e) {
            throw new ArticleNotFoundException(e.getMessage());
        }
    }

    @Override
    public Comment findById(CommentId commentId) {
        try {
            return jdbi.inTransaction((Handle handle) -> {
                Map<String, Object> result =
                    handle.createQuery("SELECT id, article_id, text FROM comment WHERE id = :id")
                        .bind("id", commentId.getValue())
                        .registerColumnMapper(new SqlArrayMapperFactory())
                        .mapToMap()
                        .first();

                return new Comment(
                    new CommentId((Long) result.get("id")),
                    new ArticleId((Long) result.get("article_id")),
                    (String) result.get("text")
                );
            });
        } catch (Exception e) {
            throw new ArticleNotFoundException(e.getMessage());
        }
    }

    @Override
    public CommentId create(String text, ArticleId articleId) {
            return jdbi.inTransaction((Handle handle) -> {
                ResultBearing resultBearing = handle.createUpdate(
                        "INSERT INTO comment (article_id, text) VALUES (:article_id, :tags)")
                                                  .bind("article_id", articleId.getValue())
                                                  .bind("tags", text)
                                                  .executeAndReturnGeneratedKeys("id");
                Map<String, Object> mapResult = resultBearing.mapToMap().first();
                return new CommentId((Long) mapResult.get("id"));
            });
    }

    @Override
    public void update(Comment comment) {
        jdbi.inTransaction((Handle handle) ->
            handle.createUpdate("UPDATE comment SET article_id = :articleId, text = :text WHERE id = :id")
                .bind("id", comment.getId().getValue())
                .bind("articleId", comment.getArticleId().getValue())
                .bind("text", comment.getText())
                .execute()
        );
    }

    @Override
    public void delete(CommentId commentId) {
        jdbi.inTransaction((Handle handle) ->
            handle.createUpdate("DELETE FROM comment WHERE id = :id")
                .bind("id", commentId.getValue())
                .execute()
        );
    }
}
