package repository;

import java.util.List;

import entity.Article.ArticleId;
import entity.Comment;
import entity.Comment.CommentId;

public interface CommentRepository {
    List<Comment> findAll();

    /**
     * @throws CommentNotFoundException
     */
    Comment findById(CommentId commentId);

    /**
     * @throws CommentIdDuplicatedException
     */
    CommentId create(String text, ArticleId articleId);

    /**
     * @throws CommentNotFoundException
     */
    void update(Comment comment);

    /**
     * @throws CommentNotFoundException
     */
    void delete(CommentId commentId);
}
