package repository.exception;

public class CommentIdDuplicatedException extends RuntimeException {
    public CommentIdDuplicatedException(String message) {
        super(message);
    }
}
