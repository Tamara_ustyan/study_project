package repository.exception;

public class ArticleIdDuplicatedException extends RuntimeException {
    public ArticleIdDuplicatedException(String message) {
        super(message);
    }
}
