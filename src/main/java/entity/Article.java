package entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import entity.Comment.CommentId;
import entity.exception.CommentNotFoundException;

public class Article {
    private final ArticleId id;
    private final String name;
    private final Set<String> tags;
    private final List<Comment> comments;

    public Article(ArticleId id, String name, Set<String> tags, List<Comment> comments) {
        this.id = id;
        this.name = name;
        this.tags = tags;
        this.comments = comments;
    }

    public Article withName(String newName) {
        return new Article(this.id, newName, this.tags, this.comments);
    }

    public Article withTags(Set<String> newTags) {
        return new Article(this.id, this.name, newTags, this.comments);
    }

    public Article withComments(List<Comment> newComments) {
        return new Article(this.id, this.name, this.tags, newComments);
    }

    public ArticleId getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<String> getTags() {
        return tags;
    }

    public List<Comment> getComments() {
        return comments;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ArticleId {
        private final long id;

        public long getValue() {
            return id;
        }

        @JsonCreator
        public ArticleId(@JsonProperty("id") long id) {
            this.id = id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ArticleId articleId = (ArticleId) o;
            return id == articleId.id;
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }
    }

    public List<CommentId> addComment(Comment comment) {
        List<Comment> newComments = new ArrayList<>(getComments());
        newComments.add(comment);
        return newComments.stream().map(Comment::getId).toList();
    }

    public List<CommentId> removeComment(CommentId commentId) {
        List<Comment> newComments = new ArrayList<>(getComments());
        newComments.remove(findCommentById(commentId));
        return newComments.stream().map(Comment::getId).toList();
    }

    private Comment findCommentById(CommentId commentId) {
        return getComments().stream().filter(comment -> comment.getId().equals(commentId)).findFirst().orElseThrow(
            () -> new CommentNotFoundException(String.format("Comment with id %s not found", commentId))
        );
    }

    @Override
    public String toString() {
        return "Article{" +
               "id=" + id +
               ", name='" + name + '\'' +
               ", tags=" + tags +
               ", comments=" + comments +
               '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Article article = (Article) o;
        return id != null && id.equals(article.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Article.class);
    }
}
