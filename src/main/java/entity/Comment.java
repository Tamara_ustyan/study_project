package entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

import entity.Article.ArticleId;

public class Comment {
    private final CommentId id;
    private final ArticleId articleId;
    private final String text;

    @JsonCreator
    public Comment(@JsonProperty("id") CommentId id,
                   @JsonProperty("articleId") ArticleId articleId,
                   @JsonProperty("text") String text) {
        this.id = id;
        this.articleId = articleId;
        this.text = text;
    }

    public Comment withText(String newText) {
        return new Comment(this.id, this.articleId, newText);
    }

    public CommentId getId() {
        return id;
    }

    public ArticleId getArticleId() {
        return articleId;
    }

    public String getText() {
        return text;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CommentId {
        private final long id;

        public long getValue() {
            return id;
        }

        @JsonCreator
        public CommentId(@JsonProperty("id") long id) {
            this.id = id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CommentId commentId = (CommentId) o;
            return id == commentId.id;
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }
    }

    @Override
    public String toString() {
        return "Comment{" +
               "id=" + id +
               ", articleId=" + articleId +
               ", text='" + text + '\'' +
               '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Comment comment = (Comment) o;
        return id != null && id.equals(comment.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Comment.class);
    }
}
