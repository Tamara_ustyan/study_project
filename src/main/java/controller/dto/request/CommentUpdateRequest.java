package controller.dto.request;

import entity.Comment.CommentId;

public record CommentUpdateRequest(
    CommentId commentId,
    String text
) {
}
