package controller.dto.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

import entity.Article.ArticleId;
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommentCreateRequest {
    private final ArticleId articleId;
    private final String text;

    @JsonCreator
    public CommentCreateRequest(
        @JsonProperty("articleId") ArticleId articleId,
        @JsonProperty("text") String text
    ) {
        this.articleId = articleId;
        this.text = text;
    }

    public ArticleId getArticleId() {
        return articleId;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "CommentCreateRequest{" +
               "articleId=" + articleId +
               ", text='" + text + '\'' +
               '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentCreateRequest that = (CommentCreateRequest) o;
        return articleId.equals(that.articleId) && text.equals(that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(articleId, text);
    }
}
