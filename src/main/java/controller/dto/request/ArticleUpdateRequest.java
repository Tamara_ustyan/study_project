package controller.dto.request;

import java.util.List;
import java.util.Set;

import entity.Article.ArticleId;
import entity.Comment.CommentId;

public record ArticleUpdateRequest(
    ArticleId articleId,
    String name,
    Set<String> tags,
    List<CommentId> comments
) {
}
