package controller.dto.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import entity.Comment;

public class ArticleCreateRequest {
    private final String name;
    private final Set<String> tags;
    private final List<Comment> comments;

    @JsonCreator
    public ArticleCreateRequest(
        @JsonProperty("name") String name,
        @JsonProperty("tags") Set<String> tags,
        @JsonProperty("comments") List<Comment> comments
    ) {
        this.name = name;
        this.tags = tags;
        this.comments = comments;
    }

    public String getName() {
        return name;
    }

    public Set<String> getTags() {
        return tags;
    }

    public List<Comment> getComments() {
        return comments;
    }

    @Override
    public String toString() {
        return "ArticleCreateRequest{" +
               "name='" + name + '\'' +
               ", tags=" + tags +
               ", comments=" + comments +
               '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArticleCreateRequest that = (ArticleCreateRequest) o;
        return name.equals(that.name) && tags.equals(that.tags) && comments.equals(that.comments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, tags, comments);
    }
}
