package controller.dto.response;

public record CommentCreateResponse(
    long id
) {
}
