package controller.dto.response;

import java.util.List;
import java.util.Set;

import entity.Article;

public record ArticleResponse(
    long articleId,
    String name,
    Set<String> tags,
    List<CommentResponse> comments
) {
    public ArticleResponse(Article article) {
        this(
            article.getId().getValue(),
            article.getName(),
            article.getTags(),
            article.getComments().stream().map(
                comment -> new CommentResponse(
                    comment.getId().getValue(),
                    comment.getArticleId().getValue(),
                    comment.getText()
                )).toList()
        );
    }
}
