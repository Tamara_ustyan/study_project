package controller.dto.response;

import entity.Comment;

public record CommentResponse(
    long commentId,
    long articleId,
    String text
) {
    public CommentResponse(Comment comment) {
        this(
            comment.getId().getValue(),
            comment.getArticleId().getValue(),
            comment.getText()
        );
    }
}
