package controller.dto.response;

public record ArticleCreateResponse(
    long id
) {
}
