package controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import controller.dto.request.CommentCreateRequest;
import controller.dto.response.CommentCreateResponse;
import controller.exception.ErrorResponse;
import entity.Comment.CommentId;
import service.CommentService;
import service.exception.CommentCreateException;
import spark.Request;
import spark.Response;
import spark.Service;

public class CommentController implements Controller {
    private static final Logger LOG = LoggerFactory.getLogger(ArticleController.class);
    private final Service service;
    private final CommentService commentService;
    private final ObjectMapper objectMapper;

    public CommentController(Service service, CommentService commentService, ObjectMapper objectMapper) {
        this.service = service;
        this.commentService = commentService;
        this.objectMapper = objectMapper;
    }

    @Override
    public void initializeEndpoints() {
        createComment();
    }

    private void createComment() {
        service.post(
            "/api/comments",
            (Request request, Response response) -> {
                response.type("application/json");
                String body = request.body();
                CommentCreateRequest commentCreateRequest =
                    objectMapper.readValue(body, CommentCreateRequest.class);
                try {
                    CommentId commentId = commentService.create(
                        commentCreateRequest.getArticleId(),
                        commentCreateRequest.getText()
                    );
                    response.status(201);
                    return objectMapper.writeValueAsString(new CommentCreateResponse(commentId.getValue()));
                } catch (CommentCreateException e) {
                    LOG.warn("Cannot create comment", e);
                    response.status(400);
                    return objectMapper.writeValueAsString(new ErrorResponse(e.getMessage()));
                }
            }
        );
    }
}
