package controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import controller.dto.request.ArticleCreateRequest;
import controller.dto.request.ArticleUpdateRequest;
import controller.dto.response.ArticleCreateResponse;
import controller.dto.response.ArticleResponse;
import controller.exception.ErrorResponse;
import entity.Article.ArticleId;
import entity.Comment.CommentId;
import repository.exception.ArticleNotFoundException;
import repository.exception.CommentNotFoundException;
import service.ArticleService;
import service.exception.ArticleCreateException;
import service.exception.ArticleDeleteException;
import service.exception.ArticleUpdateException;
import spark.Request;
import spark.Response;
import spark.Service;

import static java.lang.String.format;

public class ArticleController implements Controller {
    private static final Logger LOG = LoggerFactory.getLogger(ArticleController.class);
    private final Service service;
    private final ArticleService articleService;
    private final ObjectMapper objectMapper;

    public ArticleController(Service service, ArticleService articleService, ObjectMapper objectMapper) {
        this.service = service;
        this.articleService = articleService;
        this.objectMapper = objectMapper;
    }

    @Override
    public void initializeEndpoints() {
        createArticle();
        getArticleById();
        getArticles();
        updateArticleById();
        deleteArticleById();
        deleteCommentFromArticle();
        addCommentToArticle();
    }

    private void createArticle() {
        service.post(
            "/api/articles",
            (Request request, Response response) -> {
                response.type("application/json");
                String body = request.body();
                ArticleCreateRequest articleCreateRequest =
                    objectMapper.readValue(body, ArticleCreateRequest.class);
                try {
                    long articleId = articleService.create(
                        articleCreateRequest.getName());
                    response.status(201);
                    return objectMapper.writeValueAsString(new ArticleCreateResponse(articleId));
                } catch (ArticleCreateException e) {
                    LOG.warn("Cannot create article", e);
                    response.status(400);
                    return objectMapper.writeValueAsString(new ErrorResponse(e.getMessage()));
                }
            }
        );
    }

    private void getArticleById() {
        service.get("/api/article/:articleId",
            (Request request, Response response) -> {
                response.type("application/json");
                String articleIdParam = request.params(":articleId");
                try {
                    ArticleId articleId = new ArticleId(Long.parseLong(articleIdParam));
                    ArticleResponse article = articleService.findById(articleId);
                    response.status(200);
                    return objectMapper.writeValueAsString(article);
                } catch (ArticleNotFoundException e) {
                    LOG.warn("Article not found with ID: " + articleIdParam, e);
                    response.status(404);
                    return objectMapper.writeValueAsString(new ErrorResponse("Article not found"));
                }
            }
        );
    }

    private void getArticles() {
        service.get("/api/articles", (request, response) -> {
            response.type("application/json");
            List<ArticleResponse> articles = articleService.findAll();
            return objectMapper.writeValueAsString(articles);
        });
    }

    private void updateArticleById() {
        service.put("/api/article/:articleId", (request, response) -> {
            response.type("application/json");
            long articleId = Long.parseLong(request.params(":articleId"));
            String requestBody = request.body();
            ArticleUpdateRequest updateRequest = objectMapper.readValue(requestBody, ArticleUpdateRequest.class);
            try {
                articleService.update(updateRequest);
                response.status(201);
                return "Article updated";
            } catch (ArticleUpdateException e) {
                LOG.warn(format("Cannot update article %s", articleId), e);
                response.status(404);
                return objectMapper.writeValueAsString(new ErrorResponse(e.getMessage()));
            }
        });
    }

    private void deleteArticleById() {
        service.delete("/api/article/:articleId", (request, response) -> {
            response.type("application/json");
            long articleId = Long.parseLong(request.params(":articleId"));
            try {
                articleService.delete(new ArticleId(articleId));
                return "Article deleted";
            } catch (ArticleDeleteException e) {
                LOG.warn(format("Cannot delete article %s", articleId), e);
                response.status(404);
                return objectMapper.writeValueAsString(new ErrorResponse(e.getMessage()));
            }
        });
    }

    private void addCommentToArticle() {
        service.post("/api/article/:articleId/:commentId", (request, response) -> {
            response.type("application/json");
            long articleId = Long.parseLong(request.params(":articleId"));
            long commentId = Long.parseLong(request.params(":commentId"));
            try {
                articleService.addNewComment(new ArticleId(articleId), new CommentId(commentId));
                response.status(201);
                return "Comment added to the article";
            } catch (ArticleUpdateException e) {
                LOG.warn(format("Cannot update article %s", articleId), e);
                response.status(404);
                return objectMapper.writeValueAsString(new ErrorResponse(e.getMessage()));
            }
        });
    }

    private void deleteCommentFromArticle() {
        service.delete("/api/article/:articleId/comment/:commentId", (request, response) -> {
            response.type("application/json");
            long articleId = Long.parseLong(request.params(":articleId"));
            long commentId = Long.parseLong(request.params(":commentId"));
            try {
                articleService.deleteComment(new ArticleId(articleId), new CommentId(commentId));
                response.status(200);
                return "Comment deleted from the article";
            } catch (CommentNotFoundException e) {
                LOG.warn(format("Cannot delete comment %s", articleId), e);
                response.status(404);
                return objectMapper.writeValueAsString(new ErrorResponse(e.getMessage()));
            }
        });
    }
}
