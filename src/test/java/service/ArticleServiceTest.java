package service;

import org.flywaydb.core.Flyway;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import controller.dto.request.ArticleUpdateRequest;
import controller.dto.response.ArticleResponse;
import entity.Article.ArticleId;
import repository.ArticleRepository;
import repository.CommentRepository;
import repository.DBArticleRepository;
import repository.DBCommentRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Testcontainers
public class ArticleServiceTest {
    private static Jdbi jdbi;
    @Container
    public static final PostgreSQLContainer<?> POSTGRES = new PostgreSQLContainer<>("postgres:13");
    private static ArticleService articleService;
    private static CommentService commentService;

    @BeforeAll
    static void beforeAll() {
        String postgresJdbcUrl = POSTGRES.getJdbcUrl();
        Flyway flyway =
            Flyway.configure()
                .outOfOrder(true)
                .locations("classpath:db.migrations")
                .dataSource(postgresJdbcUrl, POSTGRES.getUsername(), POSTGRES.getPassword())
                .load();
        flyway.migrate();
        jdbi = Jdbi.create(postgresJdbcUrl, POSTGRES.getUsername(), POSTGRES.getPassword());
        TransactionManager transactionManager = new JdbiTransactionManager(jdbi);
        ArticleRepository articleRepository = new DBArticleRepository(jdbi);
        CommentRepository commentRepository = new DBCommentRepository(jdbi);
        commentService = new CommentService(commentRepository, transactionManager);
        articleService = new ArticleService(articleRepository, commentRepository, commentService, transactionManager);
    }

    @BeforeEach
    void beforeEach() {
        jdbi.useTransaction(handle -> handle.createUpdate("DELETE FROM article").execute());
    }

    @Test
    void shouldCreateFewArticles() {
        Set<ArticleId> articleIds = articleService.createArticles(Set.of("Article1", "Article2", "Article3"));

        Set<ArticleResponse> responses = articleIds.stream().map(articleService::findById).collect(Collectors.toSet());

        assertEquals(3, responses.size());
        assertTrue(responses.stream().map(ArticleResponse::name)
                       .collect(Collectors.toSet()).containsAll(Set.of("Article1", "Article2", "Article3")));
    }

    @Test
    void shouldCreateArticle() {
        final var articleId = articleService.create("New Article");
        ArticleResponse articleResponse = articleService.findById(new ArticleId(articleId));

        assertEquals(articleId, articleResponse.articleId());
    }

    @Test
    void shouldReturnAllArticles() {
        final var articleId1 = articleService.create("New Article 1");
        final var articleId2 = articleService.create("New Article 2");
        final var articleId3 = articleService.create("New Article 3");
        final var articleId4 = articleService.create("New Article 4");

        List<ArticleResponse> responses = articleService.findAll();

        assertEquals(4, responses.size());
        assertTrue(responses.stream().map(ArticleResponse::articleId).collect(Collectors.toSet())
                       .containsAll(Set.of(articleId1, articleId2, articleId3, articleId4)));
    }

    //TODO: исправить неправильную десериализацию Comment в DBArticleRepository.findById
    @Test
    @Disabled
    void shouldUpdateArticle() {
        final var articleId = articleService.create("New Article");

        ArticleResponse articleResponse1 = articleService.findById(new ArticleId(articleId));
        assertEquals(articleId, articleResponse1.articleId());

        final var commentId = commentService.create(new ArticleId(articleId), "Statiya - otval bashki");
        final var commentResponse = commentService.findById(commentId);

        final var articleUpdateRequest = new ArticleUpdateRequest(
            new ArticleId(articleId),
            "Edited Article",
            Set.of("tag1", "tag2"),
            List.of(commentId)
        );

        articleService.update(articleUpdateRequest);
        ArticleResponse articleResponse2 = articleService.findById(new ArticleId(articleId));

        assertEquals(articleId, articleResponse2.articleId());
        assertEquals(List.of(commentResponse), articleResponse2.comments());
    }
}
