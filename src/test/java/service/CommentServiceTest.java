package service;

import org.flywaydb.core.Flyway;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.List;

import controller.dto.request.CommentUpdateRequest;
import controller.dto.response.CommentResponse;
import entity.Article.ArticleId;
import repository.ArticleRepository;
import repository.CommentRepository;
import repository.DBArticleRepository;
import repository.DBCommentRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Testcontainers
class CommentServiceTest {
    private static Jdbi jdbi;
    @Container
    public static final PostgreSQLContainer<?> POSTGRES = new PostgreSQLContainer<>("postgres:13");
    private static ArticleService articleService;
    private static CommentService commentService;

    @BeforeAll
    static void beforeAll() {
        String postgresJdbcUrl = POSTGRES.getJdbcUrl();
        Flyway flyway =
            Flyway.configure()
                .outOfOrder(true)
                .locations("classpath:db.migrations")
                .dataSource(postgresJdbcUrl, POSTGRES.getUsername(), POSTGRES.getPassword())
                .load();
        flyway.migrate();
        jdbi = Jdbi.create(postgresJdbcUrl, POSTGRES.getUsername(), POSTGRES.getPassword());
        TransactionManager transactionManager = new JdbiTransactionManager(jdbi);
        ArticleRepository articleRepository = new DBArticleRepository(jdbi);
        CommentRepository commentRepository = new DBCommentRepository(jdbi);
        commentService = new CommentService(commentRepository, transactionManager);
        articleService = new ArticleService(articleRepository, commentRepository, commentService, transactionManager);
    }

    @BeforeEach
    void beforeEach() {
        jdbi.useTransaction(handle -> handle.createUpdate("DELETE FROM article").execute());
        jdbi.useTransaction(handle -> handle.createUpdate("DELETE FROM comment").execute());
    }

    @Test
    void shouldCreateComment() {
        final var articleId = articleService.create("New Article");
        final var commentId = commentService.create(new ArticleId(articleId), "Text");
        CommentResponse commentResponse = commentService.findById(commentId);

        assertEquals("Text", commentResponse.text());
        assertEquals(commentId.getValue(), commentResponse.commentId());
        assertEquals(articleId, commentResponse.articleId());
    }

    @Test
    void shouldDeleteComment() {
        final var articleId = articleService.create("New Article");
        final var commentId = commentService.create(new ArticleId(articleId), "Text");
        List<CommentResponse> responses1 = commentService.findAll();
        assertEquals(responses1.size(), 1);
        commentService.delete(commentId);
        List<CommentResponse> responses2 = commentService.findAll();
        assertEquals(responses2.size(), 0);
    }

    @Test
    void shouldUpdateComment() {
        final var articleId = articleService.create("New Article");
        final var commentId = commentService.create(new ArticleId(articleId), "Text");
        final var request = new CommentUpdateRequest(commentId, "New Text");
        commentService.update(request);
        final var updatedComment = commentService.findById(commentId);
        assertEquals(commentId.getValue(), updatedComment.commentId());
        assertEquals(articleId, updatedComment.articleId());
        assertEquals("New Text", updatedComment.text());
    }
}
