package controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Set;

import controller.dto.request.ArticleUpdateRequest;
import controller.dto.response.ArticleCreateResponse;
import controller.dto.response.ArticleResponse;
import controller.dto.response.CommentCreateResponse;
import entity.Article.ArticleId;
import entity.Comment.CommentId;
import service.ArticleService;
import service.CommentService;
import spark.Service;

import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpRequest.newBuilder;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ArticleControllerTest {
    private Service service;

    @BeforeEach
    void beforeEach() {
        service = Service.ignite();
    }

    @AfterEach
    void afterEach() {
        service.stop();
        service.awaitStop();
    }

    //TODO: десериализация ArticleId
    @Test
    void should201IfArticleIsSuccessfullyCreated() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        ArticleService articleService = mock(ArticleService.class);
        CommentService commentService = mock(CommentService.class);
        Application application = new Application(
            List.of(
                new ArticleController(
                    service,
                    articleService,
                    objectMapper
                ),
                new CommentController(
                    service,
                    commentService,
                    objectMapper
                )
            )
        );
        application.start();
        service.awaitInitialization();

        //Create Article

        when(articleService.create("Article")).thenReturn(1L);

        String articleRequestJson = """
            {
                "name": "Article",
                "tags": [],
                "comments": []
            }
            """;

        HttpResponse<String> articleResponse = newHttpClient()
                                                   .send(
                                                       newBuilder()
                                                           .uri(URI.create("http://localhost:%d/api/articles".formatted(service.port())))
                                                           .POST(HttpRequest.BodyPublishers.ofString(articleRequestJson))
                                                           .header("Content-Type", "application/json")
                                                           .build(),
                                                       HttpResponse.BodyHandlers.ofString(UTF_8)
                                                   );

        assertEquals(201, articleResponse.statusCode());
        ArticleCreateResponse articleCreateResponse = objectMapper.readValue(articleResponse.body(), ArticleCreateResponse.class);
        assertEquals(1L, articleCreateResponse.id());

        // Create Comment

        when(commentService.create(any(ArticleId.class), any(String.class))).thenReturn(new CommentId(2L));

        String commentRequestJson = """
            {
                "articleId": "1",
                "text": "Comment"
            }
            """;

        HttpResponse<String> commentResponse = newHttpClient()
                                                   .send(
                                                       newBuilder()
                                                           .uri(URI.create("http://localhost:%d/api/comments".formatted(service.port())))
                                                           .POST(HttpRequest.BodyPublishers.ofString(commentRequestJson))
                                                           .header("Content-Type", "application/json")
                                                           .build(),
                                                       HttpResponse.BodyHandlers.ofString(UTF_8)
                                                   );

        assertEquals(201, commentResponse.statusCode());
        CommentCreateResponse commentCreateResponse = objectMapper.readValue(commentResponse.body(), CommentCreateResponse.class);
        assertEquals(2L, commentCreateResponse.id());

        // Add Comment to Article

        doNothing().when(articleService).addNewComment(any(ArticleId.class), any(CommentId.class));
        final var articleId = 1L;
        final var commentId = 2L;
        String commentAddRequestJson = """
            {
                "articleId": "1"
            }
            """;

        HttpResponse<String> commentAddResponse = newHttpClient()
                                                      .send(
                                                          newBuilder()
                                                              .uri(URI.create("http://localhost:%d/api/article/%d/%d"
                                                                                  .formatted(service.port(), articleId, commentId)))
                                                              .POST(HttpRequest.BodyPublishers.ofString(commentAddRequestJson))
                                                              .header("Content-Type", "application/json")
                                                              .build(),
                                                          HttpResponse.BodyHandlers.ofString(UTF_8)
                                                      );
        assertEquals(201, commentAddResponse.statusCode());

        //Update Article

        doNothing().when(articleService).update(any(ArticleUpdateRequest.class));

        String articleUpdateRequestJson = """
            {
                "articleId": "1",
                "name": "UpdatedArticle",
                "tags": ["tag1", "tag2", "tag3"],
                "comments": ["2"]
            }
            """;

        HttpResponse<String> articleUpdateResponse = newHttpClient()
                                                         .send(
                                                             newBuilder()
                                                                 .uri(URI.create("http://localhost:%d/api/article/%d"
                                                                                     .formatted(service.port(), articleId)))
                                                                 .PUT(HttpRequest.BodyPublishers.ofString(articleUpdateRequestJson))
                                                                 .header("Content-Type", "application/json")
                                                                 .build(),
                                                             HttpResponse.BodyHandlers.ofString(UTF_8)
                                                         );
        assertEquals(201, articleUpdateResponse.statusCode());

        //Delete Comment

        doNothing().when(articleService).deleteComment(any(ArticleId.class), any(CommentId.class));

        HttpResponse<String> commentDeleteResponse = newHttpClient()
                                                         .send(
                                                             newBuilder()
                                                                 .uri(URI.create("http://localhost:%d/api/article/%d/comment/%d"
                                                                                     .formatted(service.port(), articleId, commentId)))
                                                                 .DELETE()
                                                                 .header("Content-Type", "application/json")
                                                                 .build(),
                                                             HttpResponse.BodyHandlers.ofString(UTF_8)
                                                         );
        assertEquals(200, commentDeleteResponse.statusCode());

        //Get Article

        when(articleService.findById(any(ArticleId.class))).thenReturn(new ArticleResponse(
                1L,
                "Article",
                Set.of("tag1", "tag2", "tag3"),
                emptyList()
            )
        );

        HttpResponse<String> findAccountByIdResponse = newHttpClient()
                                                         .send(
                                                             newBuilder()
                                                                 .uri(URI.create("http://localhost:%d/api/article/%d"
                                                                                     .formatted(service.port(), articleId)))
                                                                 .GET()
                                                                 .header("Content-Type", "application/json")
                                                                 .build(),
                                                             HttpResponse.BodyHandlers.ofString(UTF_8)
                                                         );
        assertEquals(200, findAccountByIdResponse.statusCode());
        ArticleResponse response = objectMapper.readValue(findAccountByIdResponse.body(), ArticleResponse.class);
        assertEquals(1L, response.articleId());
        assertEquals("Article", response.name());
        assertEquals(3, response.tags().size());
        assertTrue(response.tags().containsAll(Set.of("tag1", "tag2", "tag3")));
    }
}
