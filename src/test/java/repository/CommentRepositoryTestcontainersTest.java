package repository;

import org.flywaydb.core.Flyway;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.List;
import java.util.Set;

import entity.Article.ArticleId;
import entity.Comment;
import entity.Comment.CommentId;

import static java.util.stream.Collectors.toSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Testcontainers
class CommentRepositoryTestcontainersTest {
    @Container
    public static final PostgreSQLContainer<?> POSTGRES = new PostgreSQLContainer<>("postgres:13");

    private static Jdbi jdbi;
    private static CommentRepository commentRepository;
    private static ArticleRepository articleRepository;

    @BeforeAll
    static void beforeAll() {
        String postgresJdbcUrl = POSTGRES.getJdbcUrl();
        Flyway flyway =
            Flyway.configure()
                .outOfOrder(true)
                .locations("classpath:db.migrations")
                .dataSource(postgresJdbcUrl, POSTGRES.getUsername(), POSTGRES.getPassword())
                .load();
        flyway.migrate();
        jdbi = Jdbi.create(postgresJdbcUrl, POSTGRES.getUsername(), POSTGRES.getPassword());
        commentRepository = new DBCommentRepository(jdbi);
        articleRepository = new DBArticleRepository(jdbi);
    }

    @BeforeEach
    void beforeEach() {
        jdbi.useTransaction(handle -> handle.createUpdate("DELETE FROM article").execute());
        jdbi.useTransaction(handle -> handle.createUpdate("DELETE FROM comment").execute());
    }

    @Test
    void shouldCreateNewComment() {
        ArticleId articleId = articleRepository.create("New Article");
        CommentId commentId = commentRepository.create("Text", articleId);

        Comment comment = commentRepository.findById(commentId);
        assertEquals("Text", comment.getText());
        assertEquals(commentId, comment.getId());
        assertEquals(articleId, comment.getArticleId());
    }

    @Test
    void shouldFindAllComments() {
        ArticleId articleId1 = articleRepository.create("New Article 1");
        ArticleId articleId2 = articleRepository.create("New Article 2");
        CommentId commentId1 = commentRepository.create("Text1", articleId1);
        CommentId commentId2 = commentRepository.create("Text2", articleId1);
        CommentId commentId3 = commentRepository.create("Text3", articleId2);

        List<Comment> comments = commentRepository.findAll();
        assertEquals(3, comments.size());

        assertTrue(
            comments
                .stream()
                .map(Comment::getId)
                .collect(toSet())
                .containsAll(Set.of(commentId1, commentId2, commentId3))
        );
    }

    @Test
    void shouldUpdateComment() {
        ArticleId articleId = articleRepository.create("New Article");
        CommentId commentId = commentRepository.create("Text", articleId);
        final var comment = new Comment(commentId, articleId, "New Text");
        commentRepository.update(comment);

        Comment updatedComment = commentRepository.findById(commentId);
        assertEquals("New Text", updatedComment.getText());
        assertEquals(commentId, updatedComment.getId());
        assertEquals(articleId, updatedComment.getArticleId());
    }

    @Test
    void shouldDeleteComment() {
        ArticleId articleId = articleRepository.create("New Article");
        CommentId commentId = commentRepository.create("Text", articleId);
        commentRepository.delete(commentId);
        List<Comment> comments = commentRepository.findAll();
        assertEquals(0, comments.size());
    }
}
