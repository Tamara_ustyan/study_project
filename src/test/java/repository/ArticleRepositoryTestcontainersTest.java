package repository;

import org.flywaydb.core.Flyway;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.List;
import java.util.Set;

import entity.Article;
import entity.Article.ArticleId;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Testcontainers
class ArticleRepositoryTestcontainersTest {
    @Container
    public static final PostgreSQLContainer<?> POSTGRES = new PostgreSQLContainer<>("postgres:13");

    private static Jdbi jdbi;
    private static ArticleRepository articleRepository;

    @BeforeAll
    static void beforeAll() {
        String postgresJdbcUrl = POSTGRES.getJdbcUrl();
        Flyway flyway =
            Flyway.configure()
                .outOfOrder(true)
                .locations("classpath:db.migrations")
                .dataSource(postgresJdbcUrl, POSTGRES.getUsername(), POSTGRES.getPassword())
                .load();
        flyway.migrate();
        jdbi = Jdbi.create(postgresJdbcUrl, POSTGRES.getUsername(), POSTGRES.getPassword());
        articleRepository = new DBArticleRepository(jdbi);
    }

    @BeforeEach
    void beforeEach() {
        jdbi.useTransaction(handle -> handle.createUpdate("DELETE FROM article").execute());
    }

    @Test
    void shouldCreateNewArticle() {
        ArticleId articleId = articleRepository.create("New Article");

        Article article = articleRepository.findById(articleId);
        assertEquals("New Article", article.getName());
        assertEquals(articleId, article.getId());
    }

    @Test
    void shouldFindAllArticles() {
        ArticleId articleId1 = articleRepository.create("Article1");
        ArticleId articleId2 = articleRepository.create("Article2");
        ArticleId articleId3 = articleRepository.create("Article3");

        List<Article> responses = articleRepository.findAll();
        assertEquals(3, responses.size());
        assertTrue(
            responses
                .stream()
                .map(Article::getId)
                .collect(toSet())
                .containsAll(Set.of(articleId1, articleId2, articleId3))
        );
    }

    @Test
    void shouldUpdateArticle() {
        ArticleId articleId = articleRepository.create("New Article");

        final var article = new Article(articleId, "Updated Article", Set.of("tag1", "tag2"), emptyList());

        articleRepository.update(article);

        Article createdArticle = articleRepository.findById(articleId);
        assertEquals("Updated Article", createdArticle.getName());
        assertEquals(articleId, createdArticle.getId());
        assertTrue(createdArticle.getTags().containsAll(Set.of("tag1", "tag2")));
    }

    @Test
    void shouldDeleteArticle() {
        ArticleId articleId = articleRepository.create("New Article");
        List<Article> responses1 = articleRepository.findAll();
        assertEquals(1, responses1.size());
        assertEquals(articleId, responses1.get(0).getId());

        articleRepository.delete(articleId);
        List<Article> responses2 = articleRepository.findAll();
        assertEquals(0, responses2.size());
    }
}
